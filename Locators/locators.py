from selenium.webdriver.common.by import By


class Locators:
    # Main page locators
    login = (By.XPATH, "//a[@class='login']")
    home_slider = (By.XPATH, "//div[@id='homepage-slider']")
    cart = (By.XPATH, "//a[@title='View my shopping cart']")
    t_shirt = (By.CSS_SELECTOR, "div>ul>li[class='ajax_block_product "
                                "col-xs-12 col-sm-4 col-md-3 first-in-line f"
                                "irst-item-of-tablet-line first-item-of-"
                                "mobile-line']:nth-of-type(1)")
    t_shirt_add = (By.CSS_SELECTOR,
                   "#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4"
                   ".col-md-3.first-in-line.first-item-of-tablet-line.first"
                   "-item-of-mobile-line > div > div.right-block > "
                   "div.button-container > "
                   "a.button.ajax_add_to_cart_button.btn.btn-default > span")
    close_add_cart_popup = (By.CSS_SELECTOR, "span.cross")
    t_shirt_product = (By.XPATH, '//*[@id="homefeatured"]/'
                                 'li[1]/div/div[2]/div[2]/a[2]/span')

    # Login page locators
    email_create = (By.XPATH, "//input[@id='email_create']")
    create_button = (By.XPATH, "//button[@id='SubmitCreate']")
    email_sign_in = (By.XPATH, "//input[@id='email']")
    password = (By.XPATH, "//input[@id='passwd']")
    sign_in_button = (By.XPATH, "//button[@id='SubmitLogin']")
    authentication = (By.XPATH, "//h1[@class='page-heading']")
    error_create = (By.XPATH, "//*[text()='Invalid email address.']")
    passwd_error = (By.XPATH, "//div[@class='alert alert-danger']")
    home_from_login = (By.CSS_SELECTOR, "i.icon-home")

    # My Account page locators
    my_account = (By.XPATH, "//h1[@class='page-heading']")

    # Shopping cart page locators
    empty_cart_warning = (By.XPATH, "//p[@class='alert alert-warning']")
    sc_page_header = (By.XPATH, "//h1[@class='page-heading']")
    home_from_cart = (By.CSS_SELECTOR, "[class='home']")
    product_heading = (By.CLASS_NAME, "heading-counter")

    # T-Shirt page lacators
    t_shirt_name = (By.CSS_SELECTOR, "div>h1")
    wish_list_button = (By.CSS_SELECTOR, "a[title='Add to my wishlist']")
    add_wish_list_poup = (By.CSS_SELECTOR, "p.fancybox-error")
