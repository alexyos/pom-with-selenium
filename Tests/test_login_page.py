from Locators.locators import Locators
from selenium.common.exceptions import NoSuchElementException
from pages.login_page import LoginPage

link = "http://automationpractice.com/index.php?controller=authentication" \
       "&back=my-account "


def test_login_page_loaded(browser):
    login_page = LoginPage(browser, link)
    login_page.open(link)
    try:
        login_page.page_element(*Locators.authentication)
    except NoSuchElementException:
        print("Page didn't load")


def test_sign_up_form_fail(browser):
    login_page = LoginPage(browser, link)
    login_page.open(link)
    login_page.sign_up_form_fail()
    element = login_page.page_element(*Locators.error_create)
    assert element.text == "Invalid email address.", "Unexpected error message"


def test_sign_in_form_fail(browser):
    login_page = LoginPage(browser, link)
    login_page.open(link)
    login_page.sign_in_form_fail()
    element = login_page.page_element(*Locators.passwd_error)
    assert "Authentication failed." in element.text, "Error message"


def test_sign_in(browser):
    login_page = LoginPage(browser, link)
    login_page.open(link)
    login_page.sign_in()
    element = login_page.page_element(*Locators.my_account)
    assert "MY ACCOUNT" in element.text, "Error message"
