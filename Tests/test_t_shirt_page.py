from pages.login_page import LoginPage
from pages.main_page import MainPage
from pages.t_shirt_page import TShirtProductPage

link = "http://automationpractice.com/index.php?id_product=1&controller" \
       "=product"


def test_product_page_loaded(browser):
    t_s_page = TShirtProductPage(browser, link)
    t_s_page.open(link)
    element = t_s_page.should_show_product_name()
    assert "Faded Short Sleeve T-shirts" in element.text


def test_add_to_wish_list(browser):
    url = "http://automationpractice.com/index.php?" \
          "controller=authentication&back=my-account"
    login_page = LoginPage(browser, url)
    login_page.open(url)
    login_page.sign_in()
    login_page.home_from_login()
    link1 = browser.current_url
    main_page = MainPage(browser, link1)
    t_s_page = main_page.open_t_shirt_product_page()
    t_s_page.add_to_wish_list()
    element = t_s_page.add_to_wish_popup()
    assert "Added to your wishlist." in element.text, "Error:"
