from pages.shopping_cart_page import ShoppingCartPage

link = "http://automationpractice.com/index.php?controller=order"


def test_shopping_cart_page_loaded(browser):
    cart_page = ShoppingCartPage(browser, link)
    cart_page.open(link)
    element = cart_page.should_show_summary()
    assert element.text == "SHOPPING-CART SUMMARY", "Unexpected Error"
