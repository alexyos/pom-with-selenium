from pages.main_page import MainPage
from Locators.locators import Locators
from selenium.common.exceptions import NoSuchElementException
from pages.shopping_cart_page import ShoppingCartPage

link = "http://automationpractice.com/index.php"


def test_main_page_loaded(browser):
    main_page = MainPage(browser, link)
    main_page.open(link)
    try:
        main_page.page_element(*Locators.home_slider)
    except NoSuchElementException:
        print("Page didn't load")


def test_user_can_open_login_page(browser):
    main_page = MainPage(browser, link)
    main_page.open(link)
    login_page = main_page.open_login_page()
    element = login_page.should_be_login_page()
    assert element.text == "AUTHENTICATION", "Unexpected Error"


def test_user_can_open_shopping_cart_page(browser):
    main_page = MainPage(browser, link)
    main_page.open(link)
    shopping_cart = main_page.open_shopping_cart_page()
    element = shopping_cart.should_be_empty_cart_page()
    assert element.text == "Your shopping cart is empty.", "Couldn't find " \
                                                           "element"


def test_add_item_shopping_cart(browser):
    main_page = MainPage(browser, link)
    main_page.open(link)
    main_page.add_item_to_cart()
    url = browser.current_url
    cart_page = ShoppingCartPage(browser, url)
    element = cart_page.head_product_counter()
    assert "Your shopping cart contains: 1 Product" in element.text, \
        "Unexpected Error"


def test_user_open_t_shirt_page(browser):
    main_page = MainPage(browser, link)
    main_page.open(link)
    product_page = main_page.open_t_shirt_product_page()
    element = product_page.should_show_product_name()
    assert "Faded Short Sleeve T-shirts" in element.text, "Error message:"
