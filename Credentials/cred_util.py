import xml.etree.ElementTree as ET

tree = ET.parse('/Credentials/credentials.xml')
root = tree.getroot()


class Credentials:

    @staticmethod
    def email_real():
        for user in root:
            if user.attrib['id'] == 'real':
                for i in user:
                    if i.tag == 'email':
                        return i.text

    @staticmethod
    def passwd_real():
        for user in root:
            if user.attrib['id'] == 'real':
                for i in user:
                    if i.tag == 'password':
                        return i.text

    @staticmethod
    def email_fake():
        for user in root:
            if user.attrib['id'] == 'fake':
                for i in user:
                    if i.tag == 'email':
                        return i.text

    @staticmethod
    def passwd_fake():
        for user in root:
            if user.attrib['id'] == 'fake':
                for i in user:
                    if i.tag == 'password':
                        return i.text

    @staticmethod
    def not_email():
        for user in root:
            if user.attrib['id'] == 'fake':
                for i in user:
                    if i.tag == 'not_email':
                        return i.text
