from pages.base_page import BasePage
from Locators.locators import Locators
from Credentials.cred_util import Credentials


class LoginPage(BasePage):
    def should_be_login_page(self):
        header = self.page_element(*Locators.authentication)
        return header

    def sign_up_form_fail(self):
        email_field = self.page_element(*Locators.email_create)
        email_field.clear()
        email = Credentials.not_email()
        email_field.send_keys(email)
        button = self.page_element(*Locators.create_button)
        button.click()

    def sign_in_form_fail(self):
        email_field = self.page_element(*Locators.email_sign_in)
        email_field.clear()
        email_field.send_keys(Credentials.email_fake())
        password_field = self.page_element(*Locators.password)
        password_field.clear()
        password_field.send_keys(Credentials.passwd_fake())
        button = self.page_element(*Locators.sign_in_button)
        button.click()

    def sign_in(self):
        email_field = self.page_element(*Locators.email_sign_in)
        email_field.clear()
        email_field.send_keys(Credentials.email_real())
        password_field = self.page_element(*Locators.password)
        password_field.clear()
        password_field.send_keys(Credentials.passwd_real())
        button = self.page_element(*Locators.sign_in_button)
        button.click()

    def home_from_login(self):
        home_button = self.page_element(*Locators.home_from_login)
        home_button.click()
