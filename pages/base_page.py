class BasePage:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self, url):
        self.driver.get(url)

    def page_element(self, find_by, selector):
        element = self.driver.find_element(find_by, selector)
        return element
