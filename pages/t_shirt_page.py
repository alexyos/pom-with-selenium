from pages.base_page import BasePage
from Locators.locators import Locators


class TShirtProductPage(BasePage):
    def should_show_product_name(self):
        element = self.page_element(*Locators.t_shirt_name)
        return element

    def add_to_wish_list(self):
        element = self.page_element(*Locators.wish_list_button)
        element.click()

    def add_to_wish_popup(self):
        element = self.page_element(*Locators.add_wish_list_poup)
        return element
