from pages.base_page import BasePage
from Locators.locators import Locators


class ShoppingCartPage(BasePage):
    def should_be_empty_cart_page(self):
        element = self.page_element(*Locators.empty_cart_warning)
        return element

    def should_show_summary(self):
        element = self.page_element(*Locators.sc_page_header)
        return element

    def head_product_counter(self):
        element = self.page_element(*Locators.product_heading)
        return element
