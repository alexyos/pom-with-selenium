from selenium.webdriver import ActionChains
from pages.base_page import BasePage
from Locators.locators import Locators
from pages.login_page import LoginPage
from pages.shopping_cart_page import ShoppingCartPage
from pages.t_shirt_page import TShirtProductPage


class MainPage(BasePage):
    def open_login_page(self):
        login_link = self.page_element(*Locators.login)
        login_link.click()
        return LoginPage(self.driver, self.url)

    def open_shopping_cart_page(self):
        cart_link = self.page_element(*Locators.cart)
        cart_link.click()
        return ShoppingCartPage(self.driver, self.url)

    def add_item_to_cart(self):
        element = self.page_element(*Locators.t_shirt)
        ActionChains(self.driver).move_to_element(element).perform()
        button = self.page_element(*Locators.t_shirt_add)
        button.click()
        close_popup = self.page_element(*Locators.close_add_cart_popup)
        close_popup.click()
        self.open_shopping_cart_page()

    def open_t_shirt_product_page(self):
        element = self.page_element(*Locators.t_shirt)
        ActionChains(self.driver).move_to_element(element).perform()
        button = self.page_element(*Locators.t_shirt_product)
        button.click()
        return TShirtProductPage(self.driver, self.url)
